var data = [
    {title:" LocalHost ", website:"http://localhost:3000/"},
    {title:" GitHub ", website:"https://github.com/"},
    {title:" Bitbucket ", website:"https://bitbucket.org/dashboard/overview"},
    {title:" Gmail ", website:"https://accounts.google.com/signin/v2/identifier?passive=1209600&continue=https%3A%2F%2Faccounts.google.com%2Fb%2F0%2FAddMailService&followup=https%3A%2F%2Faccounts.google.com%2Fb%2F0%2FAddMailService&flowName=GlifWebSignIn&flowEntry=ServiceLogin"},
    {title:" Youtube ", website:"https://www.youtube.com/"},
    {title:" Netlify ", website:"https://app.netlify.com/teams/rahulranjan1895/overview"},
    {title:" JSON Convertor ", website:"https://www.convertsimple.com/convert-javascript-to-json/"},
    {title:" Jenkins ", website:"http://localhost:8080/"},
    {title:" sudheerj/javascript ", website:"https://github.com/sudheerj/javascript-interview-questions"},
    {title:" sudheerj/reactjs ", website:"https://github.com/sudheerj/reactjs-interview-questions"},
    {title:" Canva CV  ", website:"https://www.canva.com/design/DAEqta0SlfE/l8y9bICPnbvvbJcTh48Mlw/edit"},  
    {title:" Codesandbox  ", website:"https://codesandbox.io/"}, 
    {title:" Rahul Portfolio  ", website:"https://rahul-ranjan-portfolio.netlify.app/"}, 
    {title:" Rahul CV Latex  ", website:"https://www.overleaf.com/project/6156d32fc9389267ce18e642"}, 
]


export default data ;