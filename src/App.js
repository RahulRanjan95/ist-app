 import data from './data';

 function App() {

  
  return (
    <>
    <h3>Hello Rahul</h3>
    <h4>Click on website to visit.</h4>
    {data.map((data) =>  (<div key={data.title}><a href={data.website} target="_blank" rel="noopener noreferrer"><span>{data.title}{ ' '}</span></a></div>))
    }
    </>
  );
}

export default App;
